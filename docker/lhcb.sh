for i in /cvmfs/lhcb.cern.ch/lib/etc/cern_profile.d/*.sh ; do
  if  [ -r "$i" ] ; then
    . "$i"
  fi
done

if [ -z "$LHCB_ENV_MODE" ] ; then
  if [ -z "$NO_LBLOGIN" ] ; then
    LHCB_ENV_MODE=lbenv
  else
    LHCB_ENV_MODE=none
  fi
fi

if [ "$LHCB_ENV_MODE" = lbenv ] ; then
  LHCB_ENV_MODE=lbenv-stable
elif [ "$LHCB_ENV_MODE" = lbenv-dev ] ; then
  LHCB_ENV_MODE=lbenv-testing
fi

if [ -z "${QUIET_ENV}" -a -n "${QUIET_LBLOGIN}" ] ; then
  export QUIET_ENV=${QUIET_LBLOGIN}
fi

case "$LHCB_ENV_MODE" in
  none)
    env_script=
    ;;
  lbenv-*)
    env_script=/cvmfs/lhcb.cern.ch/lib/${LHCB_ENV_MODE/lbenv-/LbEnv-}.sh
    env_opts="--sh ${CMTCONFIG:+--platform=${CMTCONFIG}} ${env_opts}"
    ;;
  lblogin-dev)
    env_script=/cvmfs/lhcb.cern.ch/lib/LbLoginDev.sh
    env_opts="--no-userarea ${QUIET_ENV:+--silent} ${env_opts}"
    ;;
  lblogin|*)
    env_script=/cvmfs/lhcb.cern.ch/lib/LbLogin.sh
    env_opts="--no-userarea ${QUIET_ENV:+--silent} ${env_opts}"
    ;;
esac

if [ -n "$env_script" ] ; then
  if [ -e "$env_script" ] ; then
    . "$env_script" $env_opts
  else
    echo "WARNING: LHCb environment no avilable"
  fi
fi

# we do not want this variable to be exposed in the user environment
unset QUIET_ENV
