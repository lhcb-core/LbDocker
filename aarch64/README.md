This directory contains a [Podman](https://podman.io/) (a Docker alternative) configuration file to build a container usable for LHCb builds on aarch64-centos7.

The result is not equivalent to a centos7-build:v4 x86_64 image, but it should be good enough to be used in the nightlies.

Note: until I figure out how to deploy multiarch containers to Gitlab I'm tagging the produced image as `aarch64-centos7-build:$(date -I)` and `aarch64-centos7-build:latest`.

## Build and publish instructions
```bash
# set it to the user name to use for Gitlab
my_user=someuser

repo_root=gitlab-registry.cern.ch/lhcb-core/lbdocker
image_name=aarch64-centos7-build

ssh lblocal@lblhcbinter02
ssh -D 5555 -f -n -N -o ExitOnForwardFailure=yes ${my_user}@lxtunnel
export http_proxy=socks5://localhost:5555
export https_proxy=socks5://localhost:5555
podman build --network=host --tag ${image_name} .
podman push --creds ${my_user} ${image_name} ${repo_root}/${image_name}
podman push --creds ${my_user} ${image_name} ${repo_root}/${image_name}:$(date -I)
```
